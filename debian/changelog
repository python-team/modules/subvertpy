subvertpy (0.11.0-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Re-export upstream signing key without extra signatures.
  * Fix field name typo in debian/upstream/metadata (Repository-Browser =>
    Repository-Browse).
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.

  [ Jelmer Vernooĳ ]
  * New upstream release.

  [ Andreas Tille ]
  * Fix pydoctor input file and command line
    Closes: #1009423
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  Avoid explicitly specifying -Wl,--as-needed linker flag.
  * watch file standard 4 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 19 Apr 2024 10:31:09 +0200

subvertpy (0.11.0~git20191228+2423bf1-5) unstable; urgency=medium

  * debian/rules
    - install a manpage named subvertpy3-fast-export.1, to match the binary
      name; Closes: #966323

 -- Sandro Tosi <morph@debian.org>  Sun, 26 Jul 2020 15:58:37 -0400

subvertpy (0.11.0~git20191228+2423bf1-4) unstable; urgency=medium

  * Drop python2 support; Closes: #938579

 -- Sandro Tosi <morph@debian.org>  Sat, 25 Jul 2020 21:02:28 -0400

subvertpy (0.11.0~git20191228+2423bf1-3) unstable; urgency=medium

  * Drop python-docutils from b-d, no longer needed

 -- Sandro Tosi <morph@debian.org>  Sat, 18 Apr 2020 00:29:06 -0400

subvertpy (0.11.0~git20191228+2423bf1-2) unstable; urgency=medium

  * Team upload.
  * debian/gbp.conf: Remove incorrect debian-branch variable
    which was set to "unstable" but should actually "master" now.
  * Bump Standards-Version to 4.5.0 (no change).
  * Add "Rules-Requires-Root: no" to debian/control.
  * Remove workaround on installing our own python3-cachecontrol
    as it is now available and the latest pydoctor does depend on it.
  * Change build-dependency on python-pydoctor to Python-3-based pydoctor
  * Add build-dependency on python3-docutils.
    to parallel the existing dependency on Python-2-based python-docutils
  * debian/rules: Replace override_* with execute_after_* targets
    where appropriate.

 -- Anthony Fok <foka@debian.org>  Thu, 05 Mar 2020 19:09:25 -0700

subvertpy (0.11.0~git20191228+2423bf1-1) unstable; urgency=medium

  * Bump the snapshot’s upstream version according to setup.py.
  * Add RemoteAccess.get_url() again for backwards compatibility
    with 0.10 API to unbreak hgsubversion until it’s fixed.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 19 Jan 2020 13:24:31 +0100

subvertpy (0.10.2~git20191228+2423bf1-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot (Closes: #942678).
  * Pydoctor needs to depend on cachecontrol, but it doesn’t.
    Since python-cachecontrol has been removed from the archive,
    work this around by bringing it into the PYTHONPATH from
    the Python 3 package.
  * Tests crash with python*-dbg, disable them temporarily.

 -- Andrej Shadura <andrewsh@debian.org>  Sat, 18 Jan 2020 17:50:11 +0100

subvertpy (0.10.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 10.
  * Re-export upstream signing key without extra signatures.
  * Set upstream metadata fields: Contact.

  [ Jelmer Vernooĳ ]
  * Bump debhelper from old 10 to 12.
  * Re-export upstream signing key without extra signatures.
  * Remove obsolete fields Name, Contact from debian/upstream/metadata.
  * Drop unused dependency on python-testtools.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 16 Sep 2019 08:00:36 +0000

subvertpy (0.10.1-2) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Depend on debhelper >= 9.20151004 for SOURCE_DATE_EPOCH.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove XS-Testsuite field, not needed anymore
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

  [ Jelmer Vernooĳ ]
  * Add debian/upstream/metadata.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 26 Jul 2017 00:20:36 +0000

subvertpy (0.10.1-1) unstable; urgency=medium

  * New upstream release.
   + Fixes bigendian issues. Closes: #868770

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 19 Jul 2017 23:47:51 +0000

subvertpy (0.10.0-1) unstable; urgency=medium

  * Bump standards version to 4.0.0 (no changes).
  * Add python3-subvertpy package.
   + Use alternatives for /usr/bin/subvertpy-fast-export.
  * Migrate python-subvertpy-dbg package to python-subvertpy-dbgsym.
  * watch: Update watch file., add signing key.
  * New upstream release.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 17 Jul 2017 23:29:43 +0000

subvertpy (0.9.3-4) unstable; urgency=low

  * Use https in watch file.
  * Fix branch in Vcs-Git header.
  * Force python hash seed to 0, for reproducible pydoctor output.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Dec 2015 02:01:47 +0000

subvertpy (0.9.3-3) unstable; urgency=medium

  * Drop use of --buildtime in debian/rules, rely on SOURCE_DATE_EPOCH
    instead. Update pydoctor dependency to a version that supports
    SOURCE_DATE_EPOCH.

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 06 Dec 2015 11:57:23 +0000

subvertpy (0.9.3-2) unstable; urgency=medium

  * Build pydoctor docs with --build-time; should make build
    reproducible.

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 20 Sep 2015 21:19:03 +0000

subvertpy (0.9.3-1) unstable; urgency=medium

  * New upstream release.
   + Works around regression in subversion_fs, causing test suite failures.
     Closes: #795661

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 23 Aug 2015 13:25:14 +0000

subvertpy (0.9.1-8) unstable; urgency=medium

  * Update Vcs-Git, Vcs-Browser and Maintainer fields to reflect the
    dulwich package is now maintained by the Debian Python Modules
    packaging team.
  * Bump standards version to 3.9.6 (no changes).
  * Build with -Wl,--as-needed.
  * Build pydoctor docs with build time set to package modification
    time, to make build reproducible.

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 07 Feb 2015 17:51:51 +0100

subvertpy (0.9.1-7) unstable; urgency=low

  * Set Vcs-Git header rather than Vcs-Bzr; package has migrated to git.

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 05 Jul 2014 23:39:12 +0200

subvertpy (0.9.1-6) unstable; urgency=medium

  [ Martin Pitt ]
  * autopkgtest: Use the system-installed modules and test suite instead of
    the local source tree. Drop unnecessary "build-needed" restriction.
    (Closes: #748832)

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 24 May 2014 08:41:15 +0200

subvertpy (0.9.1-5) unstable; urgency=medium

  * Move autopkgtest restrictions to restrictions field.

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 01 Mar 2014 14:25:12 +0000

subvertpy (0.9.1-4) unstable; urgency=medium

  * Allow output on stderr during autopkgtest test runs.

 -- Jelmer Vernooij <jelmer@debian.org>  Wed, 26 Feb 2014 00:02:38 +0000

subvertpy (0.9.1-3) unstable; urgency=medium

  * Add autopkgtest support.
  * Bump standards version to 3.9.5 (no changes).

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 25 Feb 2014 02:10:49 +0000

subvertpy (0.9.1-2) unstable; urgency=low

  * Bump standards version (no changes).
  * Support dpkg-buildflags; enables hardening.

 -- Jelmer Vernooij <jelmer@debian.org>  Fri, 31 May 2013 01:18:15 +0100

subvertpy (0.9.1-1) unstable; urgency=low

  * New upstream release.
   + Fixes compatibility with newer versions of Subversion 1.7,
     disabling subvertpy.wc which is broken by svn 1.7.
     Fixes FTBFS. Closes: #680792, #680800, LP: #887749

 -- Jelmer Vernooij <jelmer@debian.org>  Thu, 01 Mar 2012 03:20:02 +0100

subvertpy (0.8.10-2) unstable; urgency=low

  * Install manual page for subvertpy-fast-export.
  * Bump standards version to 3.9.3 (no changes).
  * Fix format string of copyright file format.

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 28 Feb 2012 13:58:26 +0100

subvertpy (0.8.10-1) unstable; urgency=low

  * New upstream release.
  * Format copyright file according to DEP-5.

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 23 Jan 2012 14:23:34 +0100

subvertpy (0.8.9-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 25 Oct 2011 20:06:57 -0700

subvertpy (0.8.7-1) unstable; urgency=low

  * New upstream release.
   + Fixes double pool free, which caused testsuite failures on ia64.
     Thanks Roland Mas for the help with debugging. LP: #853960,
     Closes: #632225

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 19 Sep 2011 18:30:10 +0200

subvertpy (0.8.5-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 21 Aug 2011 03:31:12 +0200

subvertpy (0.8.4-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Fri, 19 Aug 2011 01:40:54 +0200

subvertpy (0.8.3-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 24 Jul 2011 16:30:33 +0200

subvertpy (0.8.2+bzr2368-1) unstable; urgency=low

  * New upstream snapshot.
   + Fixes several double free issues.
   + Prevents freeing RemoteAccess object before requests. LP: #803353,
     Closes: #635006
  * Fix typo in python-subvertpy-dbg package description.

 -- Jelmer Vernooij <jelmer@debian.org>  Fri, 22 Jul 2011 23:40:08 +0200

subvertpy (0.8.2+bzr2352-1) unstable; urgency=low

  * Add python-subvertpy-dbg package with debugging symbols.
  * New upstream snapshot.
   + Fixes test_transmit_text_deltas test. Closes: #625722, LP: #730931

 -- Jelmer Vernooij <jelmer@debian.org>  Fri, 17 Jun 2011 19:25:01 +0200

subvertpy (0.8.2-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Fri, 17 Jun 2011 15:11:11 +0200

subvertpy (0.8.1-1) unstable; urgency=low

  * Add missing Breaks: field to python-subvertpy.
  * Switch to debhelper 7, drop cdbs.
  * New upstream release.
  * Bump standards version to 3.9.2 (no changes).

 -- Jelmer Vernooij <jelmer@debian.org>  Thu, 02 Jun 2011 19:14:31 +0200

subvertpy (0.8.0-2) unstable; urgency=low

  * Switch to dh_python2.

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 07 Mar 2011 03:17:32 +0100

subvertpy (0.8.0-1) unstable; urgency=low

  * New upstream release.
  * Make dependency on python-testtools versioned.

 -- Jelmer Vernooij <jelmer@debian.org>  Wed, 02 Mar 2011 18:20:53 +0100

subvertpy (0.7.5-1) unstable; urgency=low

  * New upstream release.
  * Drop dependency on specific python-support. Closes: #601496

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 26 Oct 2010 11:18:48 -0700

subvertpy (0.7.4-2) unstable; urgency=low

  * Switch to python-support.
  * Build python2.5 packages for all architectures. Closes: #593750

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 04 Oct 2010 19:39:10 +0200

subvertpy (0.7.4-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.9.1 (no changes).

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 25 Sep 2010 11:02:55 -0700

subvertpy (0.7.3-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.9.0.

 -- Jelmer Vernooij <jelmer@debian.org>  Wed, 21 Jul 2010 13:23:20 +0200

subvertpy (0.7.2-3) UNRELEASED; urgency=low

  * Include API documentation. Closes: #525029
  * Bump standards version to 3.9.1 (no changes)

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 06 Dec 2010 19:01:12 +0100

subvertpy (0.7.2-2) unstable; urgency=low

  * Cope with type size changes on different platforms. Closes: #567490
  * Bump standards version to 3.8.4.
  * Switch to dpkg-source 3.0 (quilt) format

 -- Jelmer Vernooij <jelmer@debian.org>  Wed, 03 Feb 2010 15:13:51 +0100

subvertpy (0.7.2-1) unstable; urgency=low

  * New upstream release.
  * Support building when $HOME exists but is unreadable.
    Patch by Jakub Wilk. Closes: #563440

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 03 Jan 2010 17:35:38 +0100

subvertpy (0.7.1-1) unstable; urgency=low

  * New upstream release.
  * Rebuild with correct version of python-support. Closes: #560988.

 -- Jelmer Vernooij <jelmer@debian.org>  Sun, 13 Dec 2009 15:34:05 +0100

subvertpy (0.7.0-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 20 Oct 2009 01:45:26 +0200

subvertpy (0.6.9-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.8.3.

 -- Jelmer Vernooij <jelmer@debian.org>  Thu, 10 Sep 2009 17:28:15 +0200

subvertpy (0.6.8-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Thu, 18 Jun 2009 13:56:09 +0200

subvertpy (0.6.7-1) unstable; urgency=low

  * New upstream release.
  * Run testsuite.

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 01 Jun 2009 18:51:46 +0200

subvertpy (0.6.6-1) unstable; urgency=low

  * New upstream release.
  * Bump standards version to 3.8.1.

 -- Jelmer Vernooij <jelmer@debian.org>  Mon, 04 May 2009 14:29:53 +0200

subvertpy (0.6.5-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Sat, 07 Mar 2009 01:56:31 +0100

subvertpy (0.6.4-1) unstable; urgency=low

  * New upstream release.

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 17 Feb 2009 23:14:36 +0100

subvertpy (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * Add watch file.

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 27 Jan 2009 04:25:49 +0100

subvertpy (0.6.0+bzr1991-1) unstable; urgency=low

  * New upstream snapshot.

 -- Jelmer Vernooij <jelmer@debian.org>  Wed, 14 Jan 2009 20:27:30 +0100

subvertpy (0.6.0+bzr1986-1) unstable; urgency=low

  * Initial release. (Closes: #511813)

 -- Jelmer Vernooij <jelmer@debian.org>  Tue, 13 Jan 2009 17:42:24 +0100
