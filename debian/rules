#!/usr/bin/make -f

CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)

# Get the supported Python versions
PY3VERS = $(shell py3versions -r -v)

# Callable functions to determine the correct PYTHONPATH
pythonpath = $$(ls -d $(CURDIR)/build/lib.*-$(1))

export PYBUILD_NAME=subvertpy
export HOME=$(CURDIR)/nonexistent

%:
	CPPFLAGS="$(CPPFLAGS)" CFLAGS="$(CFLAGS)" CXXFLAGS="$(CXXFLAGS)" \
	LDFLAGS="$(LDFLAGS)" dh $* --with python3 --buildsystem=pybuild

execute_after_dh_auto_build:
	PYTHONHASHSEED=0 $(MAKE) pydoctor

execute_after_dh_auto_install:
	mkdir -p debian/python3-subvertpy/usr/share/doc/python3-subvertpy
	cp -a apidocs debian/python3-subvertpy/usr/share/doc/python3-subvertpy/api
	mv debian/python3-subvertpy/usr/bin/subvertpy-fast-export \
	   debian/python3-subvertpy/usr/bin/subvertpy3-fast-export

override_dh_strip:
	dh_strip -p python3-subvertpy

override_dh_installman:
	cp man/subvertpy-fast-export.1 man/subvertpy3-fast-export.1
	dh_installman man/subvertpy3-fast-export.1
	rm -f man/subvertpy3-fast-export.1
